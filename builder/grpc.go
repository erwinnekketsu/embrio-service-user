package builder

import (
	"encoding/json"
	"strconv"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

// Grpc struct grpc builder
type Grpc struct{}

// NewGrpc for initiate builder func
func NewGrpc() *Grpc {
	return &Grpc{}
}

// GetRegisterResponse generate response
func (g *Grpc) GetRegisterResponse(userToken entity.UserToken) (res *pb.RegisterResponse) {
	bytes, _ := json.Marshal(&userToken)
	_ = json.Unmarshal(bytes, &res)

	return
}

// GetDetailUserResponse generate response
func (g *Grpc) GetDetailUserResponse(data entity.DetailData) (res *pb.DetailUserResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetDetailFullUserResponse generate response
func (g *Grpc) GetDetailFullUserResponse(data entity.DetailData) (res *pb.DetailUserFullResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetDetailBusinessResponse generate response
func (g *Grpc) GetDetailBusinessResponse(data entity.BusinessProfileData) (res *pb.DetailBusinessResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetAccountResponse generate response
func (g *Grpc) GetAccountResponse(data entity.AccountInfo) (res *pb.AccountInfoResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetLocationCodeResponse generate response
func (g *Grpc) GetLocationCodeResponse(data entity.LocationId) (res *pb.GetLocationCodeResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetUserFileResponse generate response
func (g *Grpc) GetUserFileResponse(data entity.UserFile) (res *pb.UserFileResponse) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetUserAndBusinessResponse generate response
func (g *Grpc) GetUserAndBusinessResponse(data []entity.UserAndBusiness) (res []*pb.Nasabah) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetUserAdminResponse generate response
func (g *Grpc) GetUserAdminResponse(data []entity.AdminData) (res []*pb.Admin) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetUserRecommend generate response
func (g *Grpc) GetUserRecommend(data []entity.UserRecommendation) (res []*pb.UserRecommendation) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetLocationList generate response
func (g *Grpc) GetLocationList(data []entity.Location) (res []*pb.Location) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// GetSupplierList generate response
func (g *Grpc) GetSupplierList(data []entity.SupportSupplier) (res []*pb.SupportSupplier) {
	bytes, _ := json.Marshal(&data)
	_ = json.Unmarshal(bytes, &res)

	return res
}

// Register is function to register
func (g *Grpc) Register(req *pb.RegisterRequest) entity.Register {
	var userData entity.UserData
	if req.UserData != nil {
		userData = entity.UserData{
			Fullname:      req.UserData.Fullname,
			Username:      req.UserData.Username,
			PhoneNumber:   req.UserData.PhoneNumber,
			Email:         req.UserData.Email,
			Password:      req.UserData.Password,
			Timezone:      req.UserData.Timezone,
			AccountNumber: req.UserData.AccountNumber,
		}
	}

	var businessData entity.BusinessData
	if req.BusinessData != nil {
		long, _ := strconv.ParseFloat(req.BusinessData.Long, 100)
		lat, _ := strconv.ParseFloat(req.BusinessData.Lat, 100)
		businessData = entity.BusinessData{
			BusinessName:    req.BusinessData.BusinessName,
			BusinessType:    req.BusinessData.BusinessType,
			BusinessAddress: req.BusinessData.BusinessAddress,
			Province:        int(req.BusinessData.Province),
			CityDistirct:    int(req.BusinessData.CityDistirct),
			District:        int(req.BusinessData.District),
			Postcode:        int(req.BusinessData.Postcode),
			Long:            float32(long),
			Lat:             float32(lat),
		}
	}

	var userFile entity.UserFile
	if req.UserFile != nil {
		userFile = entity.UserFile{
			Siup:        req.UserFile.Siup,
			Npwp:        req.UserFile.Npwp,
			Tdp:         req.UserFile.Tdp,
			SupportFile: req.UserFile.SupportFile,
		}
	}

	return entity.Register{
		UserData:     userData,
		BusinessData: businessData,
		UserFile:     userFile,
	}
}

func (g *Grpc) UpdateUserRequest(req *pb.UpdateUserRequest) (data entity.UpdateUser) {
	data.ID = int(req.Id)
	data.Fullname = req.Fullname
	data.Username = req.Username
	data.Email = req.Email
	data.IdNumber = req.IdNumber
	data.PhoneNumber = req.PhoneNumber
	data.WANumber = req.WhatsappNumber
	data.Foto = req.Foto
	return data
}

func (g *Grpc) UpdateAccountInfoRequest(req *pb.UpdateAccountInfoRequest) (data entity.UpdateAccountInfo) {
	accountNumber, _ := strconv.Atoi(req.AccountNumber)
	data.UserID = int(req.UserId)
	data.AccountName = req.AccountName
	data.AccountNumber = int64(accountNumber)
	data.BankAccount = req.BankAccount
	return data
}

func (g *Grpc) UpdateUserPasswordRequest(req *pb.UpdateUserPasswordRequest) (data entity.UpdateUserPassword) {
	data.UserID = int(req.UserId)
	data.Password = req.Password
	data.NewPassword = req.NewPassword
	return data
}

// Register is function to register
func (g *Grpc) RegisterAdmin(req *pb.RegisterAdminRequest) (rtn entity.AdminData) {
	rtn.Email = req.Email
	rtn.Username = req.Username
	rtn.Password = req.Password
	rtn.FullName = req.FullName
	rtn.Role = int(req.Role)
	return rtn
}

// CreateSupplierRecommend is function to CreateSupplierRecommend
func (g *Grpc) CreateSupplierRecommend(id int32, supplierId int32) (rtn entity.SupplierRecommend) {
	rtn.UserId = id
	rtn.SupplierId = supplierId
	return rtn
}

func (g *Grpc) UpdateBusinessRequest(req *pb.UpdateDetailBusinessRequest) (rtn entity.BusinessProfileData) {

	rtn.ID = req.Id
	rtn.BusinessName = req.BusinessName
	rtn.BusinessDescription = req.BusinessDescription
	rtn.BusinessType = req.BusinessType
	rtn.BusinessChain = req.BusinessChain
	rtn.TotalEmployee = req.TotalEmployee
	rtn.MonthlyIncome = req.MonthlyIncome
	rtn.BusinessPicture = req.BusinessPicture
	rtn.Website = req.Website
	rtn.Facebook = req.Facebook
	rtn.Instagram = req.Instagram
	rtn.Shopee = req.Shopee
	rtn.Tokopedia = req.Tokopedia
	rtn.Bukalapak = req.Bukalapak

	return rtn
}
