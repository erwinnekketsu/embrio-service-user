package builder

import (
	"context"
	"log"
	"strconv"
	"time"

	"github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	cld "gitlab.com/erwinnekketsu/embrio-service-user.git/repo/cloudinary"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
)

type User struct {
	cloudinary *cloudinary.Cloudinary
}

func NewUser() *User {
	return &User{
		cloudinary: cld.Cld,
	}
}

func (r *User) UserDataRequest(data entity.Register) (reqUserData mysql.UserData, token string) {
	dateNow := time.Now().UTC()
	token = util.GenerateToken(data.UserData.Email)
	reqUserData.Fullname = data.UserData.Fullname
	reqUserData.AccountNumber = data.UserData.AccountNumber
	reqUserData.Email = data.UserData.Email
	reqUserData.Username = data.UserData.Username
	reqUserData.Password = util.BcryptPassword(data.UserData.Password)
	reqUserData.PhoneNumber = data.UserData.PhoneNumber
	reqUserData.CreatedAt = &dateNow
	reqUserData.Timezone = data.UserData.Timezone
	reqUserData.Token = token
	return reqUserData, token
}

func (r *User) BusinessDataRequest(userID int, data entity.Register) (reqBusinessData mysql.BusinessProfileData) {
	dateNow := time.Now().UTC()
	reqBusinessData.UserID = int32(userID)
	reqBusinessData.BusinessName = data.BusinessData.BusinessName
	reqBusinessData.BusinessType = data.BusinessData.BusinessType
	reqBusinessData.CreatedAt = &dateNow
	return reqBusinessData
}

func (r *User) BusinessAddressRequest(businessID int, data entity.Register) (reqBusinessAddress mysql.BusinessAddressData) {
	dateNow := time.Now().UTC()
	reqBusinessAddress.BusinessID = businessID
	reqBusinessAddress.Address = data.BusinessData.BusinessAddress
	reqBusinessAddress.Province = data.BusinessData.Province
	reqBusinessAddress.CityDistirct = data.BusinessData.CityDistirct
	reqBusinessAddress.District = data.BusinessData.District
	reqBusinessAddress.Postcode = data.BusinessData.Postcode
	reqBusinessAddress.Long = float64(data.BusinessData.Long)
	reqBusinessAddress.Lat = float64(data.BusinessData.Lat)
	reqBusinessAddress.CreatedAt = &dateNow

	return reqBusinessAddress
}

func (r *User) UserFileRequest(ctx context.Context, userId int, data entity.UserFile) (reqUserFile mysql.UserFile) {
	dateNow := time.Now().UTC()

	siupPublicID := "siup_" + strconv.Itoa(userId)
	npwpPublicID := "npwp_" + strconv.Itoa(userId)
	tdpPublicID := "tdp_" + strconv.Itoa(userId)
	supportFilePublicID := "support_file_" + strconv.Itoa(userId)

	siupImage, err := r.cloudinary.Upload.Upload(ctx, data.Siup, uploader.UploadParams{PublicID: siupPublicID, Folder: "siup"})
	if err != nil {
		log.Println(err)
	}
	npwpImage, _ := r.cloudinary.Upload.Upload(ctx, data.Npwp, uploader.UploadParams{PublicID: npwpPublicID, Folder: "npwp"})
	tdpImage, _ := r.cloudinary.Upload.Upload(ctx, data.Tdp, uploader.UploadParams{PublicID: tdpPublicID, Folder: "tdp"})
	supportFileImage, _ := r.cloudinary.Upload.Upload(ctx, data.SupportFile, uploader.UploadParams{PublicID: supportFilePublicID, Folder: "supportFile"})

	reqUserFile.UserID = userId
	log.Println("siupImage")
	log.Println(siupImage)
	reqUserFile.Siup = siupImage.SecureURL
	reqUserFile.Npwp = npwpImage.SecureURL
	reqUserFile.Tdp = tdpImage.SecureURL
	reqUserFile.SupportFile = supportFileImage.SecureURL
	reqUserFile.CreatedAt = &dateNow
	return reqUserFile
}

func (r *User) UserAccountRequest(userID int, data entity.UserData) (reqUserAccount mysql.UserAccount) {
	dateNow := time.Now().UTC()
	noRek, _ := strconv.Atoi(data.AccountNumber)
	reqUserAccount.AccountName = data.Fullname
	reqUserAccount.AccountNumber = noRek
	reqUserAccount.BankAccount = util.BankName
	reqUserAccount.UserID = userID
	reqUserAccount.CreatedAt = &dateNow

	return reqUserAccount
}

func (r *User) UserUpdateRequest(ctx context.Context, data entity.UpdateUser) (reqUserUpdate mysql.UpdateUser) {
	log.Println("CreateOrUpdate request: ", data)
	dateNow := time.Now().UTC()
	profilePicture := "foto_profile_" + strconv.Itoa(data.ID)
	var profilePictureImage *uploader.UploadResult
	if data.Foto != "" {
		profilePictureImage, _ = r.cloudinary.Upload.Upload(ctx, data.Foto, uploader.UploadParams{PublicID: profilePicture, Folder: "profile_picture"})
		reqUserUpdate.Foto = profilePictureImage.SecureURL
	}
	reqUserUpdate.ID = data.ID
	reqUserUpdate.Fullname = data.Fullname
	reqUserUpdate.Username = data.Username
	reqUserUpdate.Email = data.Email
	reqUserUpdate.PhoneNumber = data.PhoneNumber
	reqUserUpdate.IdNumber = data.IdNumber
	reqUserUpdate.WANumber = data.WANumber
	reqUserUpdate.UpdateAt = &dateNow
	log.Println("CreateOrUpdate response: ", reqUserUpdate)
	return reqUserUpdate
}

func (r *User) UpdateAccountInfoRequest(ctx context.Context, data entity.UpdateAccountInfo) (req mysql.UserAccount) {
	dateNow := time.Now().UTC()
	req.UserID = int(data.UserID)
	req.AccountName = data.AccountName
	req.BankAccount = data.BankAccount
	req.AccountNumber = int(data.AccountNumber)
	req.UpdatedAt = &dateNow

	return req
}

func (r *User) UpdateUserPasswordRequest(ctx context.Context, req entity.UpdateUserPassword) (res mysql.UpdateUserPassword) {
	dateNow := time.Now().UTC()
	res.UserID = req.UserID
	res.Password = util.BcryptPassword(req.NewPassword)
	res.UpdateAt = &dateNow
	return res
}

func (r *User) RegisterAdminRequest(data entity.AdminData) (reqAdminData mysql.AdminData, token string) {
	token = util.GenerateToken(data.Email)
	reqAdminData.FullName = data.FullName
	reqAdminData.Email = data.Email
	reqAdminData.Username = data.Username
	reqAdminData.Password = util.BcryptPassword(data.Password)
	reqAdminData.Token = token
	reqAdminData.IsActive = 1
	reqAdminData.Role = data.Role
	return reqAdminData, token
}

func (r *User) AdminVerify(token string) (reqAdminData mysql.AdminData) {
	reqAdminData.Token = token
	reqAdminData.IsActive = 1
	return reqAdminData
}

func (r *User) UserUpdateStatusRequest(ctx context.Context, ID int64) (reqUserUpdate mysql.UpdateUser) {
	reqUserUpdate.ID = int(ID)
	reqUserUpdate.Status = 1

	return reqUserUpdate
}

func (r *User) AdminUpdateStatusRequest(ID int32, status int32) (reqAdminUpdate mysql.AdminData) {
	reqAdminUpdate.ID = int(ID)
	reqAdminUpdate.IsActive = int(status)

	return reqAdminUpdate
}

func (r *User) SupplierRecommendRequest(ctx context.Context, data entity.SupplierRecommend) (reqSupplier mysql.SupplierRecommend) {
	reqSupplier.UserId = data.UserId
	reqSupplier.SupplierId = data.SupplierId

	return reqSupplier
}

func (r *User) DeleteRecommendRequest(ctx context.Context, userId int32) (reqSupplier mysql.SupplierRecommend) {
	reqSupplier.UserId = userId

	return reqSupplier
}

func (r *User) DeleteAlamatTokoRequest(ctx context.Context, userId int32) (reqSupplier mysql.Product) {
	reqSupplier.BusinessId = userId

	return reqSupplier
}

func (r *User) DeleteUserRequest(ctx context.Context, ID int32) (reqUserUpdate mysql.UpdateUser) {
	reqUserUpdate.ID = int(ID)

	return reqUserUpdate
}

func (r *User) UpdateBusinessRequest(ctx context.Context, data entity.BusinessProfileData) (req mysql.BusinessProfileData) {

	req.ID = data.ID
	req.BusinessName = data.BusinessName
	req.BusinessDescription = data.BusinessDescription
	req.BusinessType = data.BusinessType
	req.BusinessChain = data.BusinessChain
	req.TotalEmployee = data.TotalEmployee
	req.MonthlyIncome = data.MonthlyIncome
	if data.BusinessPicture != "" {
		businessPicture := "foto_profile_" + strconv.Itoa(int(data.ID))
		businessPictureImage, _ := r.cloudinary.Upload.Upload(ctx, data.BusinessPicture, uploader.UploadParams{PublicID: businessPicture, Folder: "profile_picture"})
		req.BusinessPicture = businessPictureImage.SecureURL
	}
	req.Website = data.Website
	req.Facebook = data.Facebook
	req.Instagram = data.Instagram
	req.Shopee = data.Shopee
	req.Tokopedia = data.Tokopedia
	req.Bukalapak = data.Bukalapak

	return req
}
