package repo

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util/errors"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	builderReg *builder.User
	iMysql     mysql.IMysql
	embrioDB   *sqlx.DB
}

func NewUser() *User {
	return &User{
		builderReg: builder.NewUser(),
		iMysql:     mysql.NewClient(),
		embrioDB:   mysql.EmbrioDB,
	}
}

func (u *User) GetUserByEmail(ctx context.Context, email string) (data mysql.UserId, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"email$eq": email,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryFindNasabah,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (u *User) GetUserByRekening(ctx context.Context, noRekening string) (data mysql.UserId, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"rekeningNasabah$eq": noRekening,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryFindNasabah,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (u *User) GetUserByNoTlp(ctx context.Context, noTlp string) (data mysql.UserId, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"noTlp$eq": noTlp,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryFindNasabah,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (u *User) GetUserById(ctx context.Context, id int) (data mysql.DetailData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetNasabah,
	)

	if err != nil {
		return data, errors.ErrBadRequest("User Not Found")
	}
	return data, nil
}

func (u *User) GetUserAccountInfo(ctx context.Context, Id int) (data mysql.UserAccount, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq": Id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetAccountInfo,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetUserFile(ctx context.Context, Id int) (data mysql.UserFile, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq": Id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetUserFile,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetLoc(ctx context.Context, name string, tag string) (data mysql.Province, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"nama$like": name,
		},
	}

	var queryString string
	if tag == "prov" {
		queryString = mysql.QueryGetPovince
	} else if tag == "kab" {
		queryString = mysql.QueryGetCity
	} else if tag == "kec" {
		queryString = mysql.QueryGetDisctrict
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		queryString,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetLocById(ctx context.Context, id int32, tag string) (data []entity.Location, err error) {
	query := &util.Query{}

	var queryString string
	if tag == "prov" {
		queryString = mysql.QueryGetPovince
	} else if tag == "kab" {
		query = &util.Query{
			Filter: map[string]interface{}{
				"idProvinsi$eq": id,
			},
		}
		queryString = mysql.QueryGetCity
	} else if tag == "kec" {
		query = &util.Query{
			Filter: map[string]interface{}{
				"idKota$eq": id,
			},
		}
		queryString = mysql.QueryGetDisctrict
	}

	var datas []mysql.Location
	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&datas,
		query,
		queryString,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	for _, v := range datas {
		dataLocation := entity.Location{
			ID:   v.ID,
			Name: v.Name,
		}
		data = append(data, dataLocation)
	}

	return data, nil
}

func (u *User) UpdateUser(ctx context.Context, data entity.UpdateUser) error {
	log.Println("CreateOrUpdate request: ", data)
	reqUserData := u.builderReg.UserUpdateRequest(ctx, data)
	res, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryUpdateNasabah)
	if err != nil {
		log.Println(err)
		return err
	}
	log.Println("CreateOrUpdate response: ", res)
	return nil
}

func (u *User) UpdateAccountInfo(ctx context.Context, data entity.UpdateAccountInfo) error {
	reqUserData := u.builderReg.UpdateAccountInfoRequest(ctx, data)
	_, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryUpdateAccountInfo)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) UpdateUserPassword(ctx context.Context, req entity.UpdateUserPassword) error {
	dataUser, err := u.GetUserById(ctx, req.UserID)
	if err != nil {
		return err
	}
	log.Println(dataUser.Password)
	password := dataUser.Password
	err = bcrypt.CompareHashAndPassword([]byte(password), []byte(req.Password))
	if err != nil {
		return errors.ErrBadRequest("Password not match")
	}

	dataPassword := u.builderReg.UpdateUserPasswordRequest(ctx, req)
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, dataPassword, mysql.QueryUpdatePassword)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (u *User) UpdateUserStatus(ctx context.Context, ID int64) error {
	reqUserData := u.builderReg.UserUpdateStatusRequest(ctx, ID)
	_, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryUpdateStatusUser)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) GetUserUserAndBusiness(ctx context.Context, page int, limit int, status int) (data []entity.UserAndBusiness, pagintaion *util.Pagination, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"status$eq!": status,
		},
	}

	// u.excel(ctx)
	// u.excelUpdateByEmail(ctx)
	datas, paginations, err := u.iMysql.FindWithPagination(
		ctx,
		u.embrioDB,
		query,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetNasabahAndBusiness,
	)

	log.Println(datas)
	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataUser := entity.UserAndBusiness{
			ID:            v.ID,
			Name:          v.Name,
			BusinessName:  v.BusinessName,
			BusinessType:  v.BusinessType,
			BusinessChain: v.BusinessChain,
			Province:      v.Province,
		}
		data = append(data, dataUser)
	}

	return data, paginations, nil
}

func (u *User) GetUserUserAndBusinessById(ctx context.Context, id int) (data mysql.UserAndBusiness, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq": id,
		},
	}

	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetNasabahAndBusiness,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetBusinessById(ctx context.Context, user_id int32, id int32) (data mysql.BusinessProfileData, err error) {
	var query *util.Query
	if id != 0 {
		query = &util.Query{
			Filter: map[string]interface{}{
				"id$eq": id,
			},
		}
	} else {
		query = &util.Query{
			Filter: map[string]interface{}{
				"idNasabah$eq": user_id,
			},
		}
	}

	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetUserBusiness,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) UpdateBusiness(ctx context.Context, data entity.BusinessProfileData) error {
	reqUserData := u.builderReg.UpdateBusinessRequest(ctx, data)
	_, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryUpdateBusiness)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) GetIdsRecommendation(ctx context.Context, id int) (ids []int, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq": id,
		},
	}

	var data []mysql.SupplierRecommend
	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetRecommendation,
	)

	if err != nil {
		return ids, errors.ErrBadRequest("User Not Found")
	}

	for _, v := range data {
		ids = append(ids, int(v.SupplierId))
	}

	return ids, nil
}

func (u *User) GetUserRecommendation(ctx context.Context, id int, tag string) (data []mysql.UserRecommendation, err error) {
	ids, err := u.GetIdsRecommendation(ctx, id)
	dataNasabah, err := u.GetUserUserAndBusinessById(ctx, id)
	lat := fmt.Sprintf("%f", dataNasabah.Lat)
	long := fmt.Sprintf("%f", dataNasabah.Long)
	var query *util.Query
	var stringQuery string
	query = &util.Query{
		Filter: map[string]interface{}{
			"tokoNasabah.jenisUsaha$eq": dataNasabah.BusinessType,
			"idNasabah$ne":              id,
		},
		Sort: "distance",
	}
	if tag == "supplier" {
		if len(ids) > 0 {
			query = &util.Query{
				Filter: map[string]interface{}{
					"idNasabah$in": ids,
				},
				Sort: "distance",
			}
		}
		stringQuery = `SELECT tokoNasabah.id AS id, nasabah.id as idNasabah, nasabah.namaLengkap as namaLengkap, tokoNasabah.namaToko as namaToko, tokoNasabah.fotoProfileToko as foto, IF(tokoNasabah.jenisUsaha != "",tokoNasabah.jenisUsaha,"") AS jenisUsaha, alamatToko.detailAlamat as detailAlamat, kabupaten_kota.nama AS kota, alamatToko.latitude as latitude, alamatToko.longitude as longitude, produk.harga AS price, produk.nama AS namaProduk, tokoNasabah.deskripsiToko AS deskripsi, SQRT(POW(69.1*(latitude-` + lat + `),2)+POW(69.1*(` + long + `-longitude)*COS(latitude/57.3),2)) AS distance FROM alamatToko JOIN tokoNasabah ON alamatToko.idToko=tokoNasabah.id JOIN nasabah ON tokoNasabah.idNasabah=nasabah.id JOIN kabupaten_kota ON alamatToko.kabupaten_kota = kabupaten_kota.id LEFT JOIN (SELECT harga, nama, idToko FROM produk, tokoNasabah WHERE tokoNasabah.id = produk.id) AS produk ON tokoNasabah.id = produk.idToko`
	} else if tag == "buyer" {
		stringQuery = `SELECT tokoNasabah.id AS id, nasabah.id as idNasabah, nasabah.namaLengkap as namaLengkap, tokoNasabah.namaToko as namaToko, nasabah.foto as foto, IF(tokoNasabah.jenisUsaha != "",tokoNasabah.jenisUsaha,"") AS jenisUsaha, alamatToko.detailAlamat as detailAlamat, kabupaten_kota.nama AS kota, alamatToko.latitude as latitude, alamatToko.longitude as longitude, tokoNasabah.deskripsiToko AS deskripsi, SQRT(POW(69.1*(latitude-` + lat + `),2)+POW(69.1*(` + long + `-longitude)*COS(latitude/57.3),2)) AS distance FROM alamatToko JOIN tokoNasabah ON alamatToko.idToko=tokoNasabah.id JOIN nasabah ON tokoNasabah.idNasabah=nasabah.id JOIN kabupaten_kota ON alamatToko.kabupaten_kota = kabupaten_kota.id`
	} else if tag == "similiar" {
		stringQuery = `SELECT tokoNasabah.id AS id, nasabah.id as idNasabah, nasabah.namaLengkap as namaLengkap, tokoNasabah.namaToko as namaToko, tokoNasabah.fotoProfileToko as foto, IF(tokoNasabah.jenisUsaha != "",tokoNasabah.jenisUsaha,"") AS jenisUsaha, alamatToko.detailAlamat as detailAlamat, kabupaten_kota.nama AS kota, alamatToko.latitude as latitude, alamatToko.longitude as longitude, tokoNasabah.deskripsiToko AS deskripsi, SQRT(POW(69.1*(latitude-` + lat + `),2)+POW(69.1*(` + long + `-longitude)*COS(latitude/57.3),2)) AS distance FROM alamatToko JOIN tokoNasabah ON alamatToko.idToko=tokoNasabah.id JOIN nasabah ON tokoNasabah.idNasabah=nasabah.id JOIN kabupaten_kota ON alamatToko.kabupaten_kota = kabupaten_kota.id`
	}

	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&data,
		query,
		stringQuery,
	)
	log.Println(err)
	if err != nil {
		return data, errors.ErrBadRequest("User Not Found")
	}

	return data, nil
}

func (u *User) GetLocationByPostcode(ctx context.Context, postcode int32) (data mysql.LocationName, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"kodepos$eq": postcode,
		},
	}

	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetLocationByPostcode,
	)

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetLocationId(ctx context.Context, postcode int32) (data entity.LocationId, err error) {
	locationName, err := u.GetLocationByPostcode(ctx, postcode)

	query := &util.Query{
		Filter: map[string]interface{}{
			"kecamatan.nama$eq": locationName.District,
		},
	}
	var datas mysql.LocationId
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&datas,
		query,
		mysql.QueryGetLocationByDsitrict,
	)

	data.CityId = datas.CityId
	data.ProvinceId = datas.ProvinceId
	data.DistrictId = datas.DistrictId

	if err != nil {
		log.Println(err)
		return data, err
	}

	return data, nil
}

func (u *User) GetSupplierList(ctx context.Context, queryFilter string) (data []mysql.SupportSupplier, err error) {
	var query *util.Query
	if queryFilter != "" {
		query = &util.Query{
			Filter: map[string]interface{}{
				// "rantaiUsaha$eq!": "Supplier",
				"namaToko$like": queryFilter,
			},
		}
	} else {
		query = &util.Query{
			Filter: map[string]interface{}{
				"rantaiUsaha$eq!": "Supplier",
			},
		}
	}

	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&data,
		query,
		mysql.QueryGetSupplierBussiness,
	)
	log.Println(err)

	if err != nil {
		return data, errors.ErrBadRequest("Supplier Not Found")
	}

	return data, nil
}

func (u *User) CreateSupplierRecommend(ctx context.Context, data entity.SupplierRecommend) error {
	reqUserData := u.builderReg.SupplierRecommendRequest(ctx, data)
	_, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryCreateRecommendation)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) DeleteSupplierRecommend(ctx context.Context, user_id int32) error {
	reqUserData := u.builderReg.DeleteRecommendRequest(ctx, user_id)
	_, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryDeleteRecommend)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) GetDataDashboard(ctx context.Context, querySort string) (data entity.DataDashboard, err error) {
	query := &util.Query{}
	var dataUser []mysql.DataDashboard
	var dataStore []mysql.DataDashboard
	var dataProduct []mysql.DataDashboard
	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&dataUser,
		query,
		mysql.QueryTotalUser,
	)
	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&dataStore,
		query,
		mysql.QueryTotalStore,
	)
	err = u.iMysql.Select(
		ctx,
		u.embrioDB,
		&dataProduct,
		query,
		mysql.QueryTotalProduct,
	)
	log.Println(err)

	totalUser := 0
	for _, v := range dataUser {
		data.DataUser = append(data.DataUser, float32(v.Total))
		var dateUsr string
		year := strconv.Itoa(int(v.Year))
		month := util.Month[int(v.Month)]
		dateUsr = month + " " + year
		data.DateUser = append(data.DateUser, dateUsr)
		totalUser += int(v.Total)
	}

	totalStore := 0
	for _, v := range dataStore {
		data.DataStore = append(data.DataStore, float32(v.Total))
		var dateStr string
		year := strconv.Itoa(int(v.Year))
		month := util.Month[int(v.Month)]
		dateStr = month + " " + year
		data.DateStore = append(data.DateStore, dateStr)
		totalStore += int(v.Total)
	}

	totalProduct := 0
	for _, v := range dataProduct {
		data.DataProduct = append(data.DataProduct, float32(v.Total))
		var dateProd string
		year := strconv.Itoa(int(v.Year))
		month := util.Month[int(v.Month)]
		dateProd = month + " " + year
		data.DateProduct = append(data.DateProduct, dateProd)
		totalProduct += int(v.Total)
	}

	data.TotalProduct = int64(totalProduct)
	data.TotalUser = int64(totalUser)
	data.TotalStore = int64(totalStore)

	if err != nil {
		return data, errors.ErrBadRequest("Supplier Not Found")
	}
	return data, nil
}

func (u *User) DetailFullUser(ctx context.Context, id int32) (data entity.DetailData, err error) {
	query := &util.Query{}
	var dataUser mysql.UserData
	var dataUserAccount mysql.UserAccount
	var dataBusinessProfileData mysql.BusinessProfileData
	var dataUserFile mysql.UserFile
	var dataBusinessAddressData mysql.BusinessAddressData
	query = &util.Query{
		Filter: map[string]interface{}{
			"id$eq!": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataUser,
		query,
		mysql.QueryGetNasabah,
	)
	log.Println(dataUser)
	query = &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq!": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataUserFile,
		query,
		mysql.QueryGetUserFile,
	)
	log.Println(dataUserFile)
	query = &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq!": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataUserAccount,
		query,
		mysql.QueryGetAccountInfo,
	)
	log.Println(dataUserAccount)
	query = &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq!": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataBusinessProfileData,
		query,
		mysql.QueryGetUserBusiness,
	)
	log.Println(dataBusinessProfileData)
	log.Println(dataBusinessProfileData.ID)
	query = &util.Query{
		Filter: map[string]interface{}{
			"idToko$eq!": dataBusinessProfileData.ID,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataBusinessAddressData,
		query,
		mysql.QueryGetNasabahDetailAddress,
	)
	log.Println(err)
	log.Println(dataBusinessAddressData)

	data.Fullname = dataUser.Fullname
	data.Username = dataUser.Username
	data.PhoneNumber = dataUser.PhoneNumber
	data.Email = dataUser.Email
	data.AccountNumber = dataUser.AccountNumber
	data.IdNumber = dataUser.IdNumber
	data.WANumber = dataUser.WANumber
	data.Foto = dataUser.Foto
	data.UserFile.Npwp = dataUserFile.Npwp
	data.UserFile.Siup = dataUserFile.Siup
	data.UserFile.SupportFile = dataUserFile.SupportFile
	data.UserFile.Tdp = dataUserFile.Tdp
	data.AccountInfo.AccountName = dataUserAccount.AccountName
	data.AccountInfo.AccountNumber = strconv.Itoa(dataUserAccount.AccountNumber)
	data.AccountInfo.BankAccount = dataUserAccount.BankAccount
	data.BusinessProfileData.Bukalapak = dataBusinessProfileData.Bukalapak
	data.BusinessProfileData.BusinessChain = dataBusinessProfileData.BusinessChain
	data.BusinessProfileData.BusinessDescription = dataBusinessProfileData.BusinessDescription
	data.BusinessProfileData.BusinessName = dataBusinessProfileData.BusinessName
	data.BusinessProfileData.BusinessPicture = dataBusinessProfileData.BusinessPicture
	data.BusinessProfileData.BusinessType = dataBusinessProfileData.BusinessType
	data.BusinessProfileData.Facebook = dataBusinessProfileData.Facebook
	data.BusinessProfileData.Instagram = dataBusinessProfileData.Instagram
	data.BusinessProfileData.MonthlyIncome = dataBusinessProfileData.MonthlyIncome
	data.BusinessProfileData.Shopee = dataBusinessProfileData.Shopee
	data.BusinessProfileData.Tokopedia = dataBusinessProfileData.Tokopedia
	data.BusinessProfileData.TotalEmployee = dataBusinessProfileData.TotalEmployee
	data.BusinessProfileData.Website = dataBusinessProfileData.Website
	data.BusinessData.BusinessAddress = dataBusinessAddressData.Address
	data.BusinessData.CityDistirctText = dataBusinessAddressData.CityDistirctText
	data.BusinessData.DistrictText = dataBusinessAddressData.DistrictText
	data.BusinessData.ProvinceText = dataBusinessAddressData.ProvinceText
	data.BusinessData.Postcode = dataBusinessAddressData.Postcode
	data.BusinessData.Long = float32(dataBusinessAddressData.Long)
	data.BusinessData.Lat = float32(dataBusinessAddressData.Lat)
	data.BusinessData.PinLocation = "https://www.google.com/maps/place/" + dataBusinessAddressData.DistrictText + ",+" + dataBusinessAddressData.CityDistirctText + ",+" + dataBusinessAddressData.ProvinceText + "/@" + fmt.Sprintln(dataBusinessAddressData.Lat) + ",+" + fmt.Sprintln(dataBusinessAddressData.Lat) + "+"

	return data, nil
}

func (u *User) DeleteUser(ctx context.Context, id int32) error {
	var dataBusinessProfileData mysql.BusinessProfileData
	query := &util.Query{}
	var err error
	query = &util.Query{
		Filter: map[string]interface{}{
			"idNasabah$eq!": id,
		},
	}
	err = u.iMysql.Get(
		ctx,
		u.embrioDB,
		&dataBusinessProfileData,
		query,
		mysql.QueryGetBusinessId,
	)
	if err != nil {
		return err
	}
	reqUserData := u.builderReg.DeleteAlamatTokoRequest(ctx, dataBusinessProfileData.ID)
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryDeleteAlamatToko)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUserData, mysql.QueryDeleteProduk)
	if err != nil {
		log.Println(err)
		return err
	}
	reqUser := u.builderReg.DeleteUserRequest(ctx, id)
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUser, mysql.QueryDeleteTokoNasabah)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUser, mysql.QueryDeleteBerkas)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUser, mysql.QueryDeleteRekeningNasabbah)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, reqUser, mysql.QueryDeleteNasabah)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (u *User) excel(ctx context.Context) {
	xlsx := excelize.NewFile()

	xlsx, err := excelize.OpenFile("../Salinan-Anggota-RB-2020-3.xlsx")
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	Data35Nasabah := "Potensi Buyer"
	dateNow := time.Now().UTC()
	rows := make([]mysql.UserData, 0)
	for i := 29; i < 65; i++ {
		location := u.GetLocation(ctx, xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("O%d", i)))
		if len(location.Response.View) == 0 {
			continue
		}
		log.Println(location.Response.View[0].Result[0].Location.Address.City)
		log.Println(location.Response.View[0].Result[0].Location.Address.PostalCode)
		log.Println(location.Response.View[0].Result[0].Location.Address.District)
		log.Println(location.Response.View[0].Result[0].Location.Address.County)
		kodepos, _ := strconv.Atoi(location.Response.View[0].Result[0].Location.Address.PostalCode)
		longitude := location.Response.View[0].Result[0].Location.DisplayPosition.Longitude
		latitude := location.Response.View[0].Result[0].Location.DisplayPosition.Latitude
		getProvId, _ := u.GetLoc(ctx, location.Response.View[0].Result[0].Location.Address.County, "prov")
		getKabId, _ := u.GetLoc(ctx, location.Response.View[0].Result[0].Location.Address.City, "kab")
		getKecId, _ := u.GetLoc(ctx, location.Response.View[0].Result[0].Location.Address.District, "kec")
		email := xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("K%d", i))
		username := strings.Split(email, "@")
		password := util.BcryptPassword(username[0] + "123")
		token := util.GenerateToken(email)
		noKtp, _ := strconv.Atoi(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("D%d", i)))
		row := mysql.UserData{
			Email:         xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("K%d", i)),
			Fullname:      xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("B%d", i)),
			IdNumber:      int64(noKtp),
			PhoneNumber:   xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("F%d", i)),
			WANumber:      xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("G%d", i)),
			AccountNumber: xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("J%d", i)),
			Username:      username[0],
			Password:      password,
			Token:         token,
			Timezone:      "+7",
			CreatedAt:     &dateNow,
			Foto:          xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("T%d", i)),
			Status:        1,
		}
		lastId, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, row, mysql.QueryCreateNasabah)
		if err != nil {
			log.Println(err)
		}
		AccountNumber := xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("J%d", i))
		var noRekening int
		bankAccount := "BRI"
		noRekening, err = strconv.Atoi(AccountNumber)
		if err != nil {
			noRekening = 0
			bankAccount = "-"
		}
		rekening := mysql.UserAccount{
			UserID:        int(lastId),
			AccountNumber: noRekening,
			BankAccount:   bankAccount,
			AccountName:   xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("B%d", i)),
			CreatedAt:     &dateNow,
		}
		_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, rekening, mysql.QueryCreateRekening)
		if err != nil {
			log.Println(err)
		}

		totalEmployee, _ := strconv.ParseFloat(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("S%d", i)), 32)
		monthlyIncome, _ := strconv.ParseFloat(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("R%d", i)), 32)
		userBusiness := mysql.BusinessProfileData{
			UserID:              int32(lastId),
			BusinessName:        xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("L%d", i)),
			BusinessDescription: xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("P%d", i)),
			BusinessType:        xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("M%d", i)),
			BusinessChain:       xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("N%d", i)),
			TotalEmployee:       int32(totalEmployee),
			MonthlyIncome:       int32(monthlyIncome),
			CreatedAt:           &dateNow,
			BusinessPicture:     xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("T%d", i)),
			Website:             xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("W%d", i)),
			Facebook:            xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("U%d", i)),
			Instagram:           xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("V%d", i)),
		}
		businessId, err := u.iMysql.CreateOrUpdate(ctx, u.embrioDB, userBusiness, mysql.QueryCreateToko)
		if err != nil {
			log.Println(err)
		}
		businessAddress := mysql.BusinessAddressData{
			BusinessID:   int(businessId),
			Province:     getProvId.ID,
			CityDistirct: getKabId.ID,
			District:     getKecId.ID,
			Postcode:     kodepos,
			Long:         longitude,
			Lat:          latitude,
			Address:      xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("O%d", i)),
			CreatedAt:    &dateNow,
		}
		_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, businessAddress, mysql.QueryCreateAlamatTokoNasabah)
		if err != nil {
			log.Println(err)
		}
		price, err := strconv.ParseFloat(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("Z%d", i)), 32)
		if err != nil {
			log.Println(err)
		}
		cat := xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("M%d", i))
		dataProduct := mysql.Product{
			Nama:        xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("Y%d", i)),
			Price:       float32(price),
			Description: xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("AA%d", i)),
			Image:       xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("T%d", i)),
			Category:    cat,
			BusinessId:  int32(businessId),
			CreatedAt:   &dateNow,
		}
		_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, dataProduct, mysql.QueryCreateProduk)
		if err != nil {
			log.Println(err)
		}
		rows = append(rows, row)
		log.Println(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("L%d", i)))
		log.Println(xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("G%d", i)))
	}

	fmt.Printf("%v \n", rows)
}

func (u *User) excelUpdateByEmail(ctx context.Context) {
	xlsx := excelize.NewFile()

	xlsx, err := excelize.OpenFile("./Salinan-Anggota-RB-2020.xlsx")
	if err != nil {
		log.Fatal("ERROR", err.Error())
	}

	Data35Nasabah := "Potensi Supplier"
	rows := make([]mysql.UserData, 0)
	for i := 2; i < 29; i++ {
		email := xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("K%d", i))
		log.Println("email")
		log.Println(email)
		user, err := u.GetUserByEmail(ctx, email)
		if err != nil {
			log.Println(err)
		}
		log.Println("user")
		log.Println(user)
		log.Println(user.ID)
		userBusiness := mysql.BusinessProfileData{
			UserID:    int32(user.ID),
			Website:   xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("W%d", i)),
			Facebook:  xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("U%d", i)),
			Instagram: xlsx.GetCellValue(Data35Nasabah, fmt.Sprintf("V%d", i)),
		}
		log.Println("userBusiness")
		log.Println(userBusiness)
		_, err = u.iMysql.CreateOrUpdate(ctx, u.embrioDB, userBusiness, mysql.QueryUpdateBusinessByEmail)
		if err != nil {
			log.Println(err)
		}
	}

	fmt.Printf("%v \n", rows)
}

func (u *User) GetLocation(ctx context.Context, address string) (data entity.Geolocation) {
	url := "https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey=IfQhTrtK6VxfS4BfHv0EbctC_n3dbd_VOuu6Ldr8dMc&searchtext=" + address
	url = strings.Replace(url, " ", "%20", 20)
	log.Println(url)

	res, getErr := http.Get(url)
	if getErr != nil {
		log.Println(getErr)
	}
	log.Println(res)
	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Println(readErr)
	}

	// data := entity.Geolocation{}
	jsonErr := json.Unmarshal(body, &data)
	if jsonErr != nil {
		log.Println(jsonErr)
	}
	log.Println(data)
	return
}
