package mysql

const (
	QueryCreateNasabah = `
		INSERT INTO nasabah (username, email, namaLengkap, noKTP, noTlp, noWA, rekeningNasabah, status, password, createdAt, timezone, token, foto) VALUES (:username, :email, :namaLengkap, :noKTP, :noTlp, :noWA, :rekeningNasabah, :status, :password, :createdAt, :timezone, :token, :foto);
	`

	QueryCreateToko = `
		INSERT INTO tokoNasabah (namaToko, deskripsiToko, jenisUsaha, rantaiUsaha, jumlahKaryawan, omsetPerbulan, fotoProfileToko, idNasabah) VALUES (:namaToko, :deskripsiToko, :jenisUsaha, :rantaiUsaha, :jumlahKaryawan, :omsetPerbulan, :fotoProfileToko, :idNasabah);
	`

	// QueryCreateToko = `
	// 	INSERT INTO tokoNasabah (namaToko, deskripsiToko, jenisUsaha, rantaiUsaha, jumlahKaryawan, omsetPerbulan, fotoProfileToko, idNasabah, website, facebook, instagram) VALUES (:namaToko, :deskripsiToko, :jenisUsaha, :rantaiUsaha, :jumlahKaryawan, :omsetPerbulan, :fotoProfileToko, :idNasabah, :website, :facebook, :instagram);
	// `

	QueryCreateTokoNasabah = `
		INSERT INTO tokoNasabah (namaToko, idNasabah, createdAt) VALUES (:namaToko,  :idNasabah, :createdAt);
	`

	QueryCreateAlamatTokoNasabah = `
		INSERT INTO alamatToko (detailAlamat, provinsi, kabupaten_kota, kecamatan, kodepos, longitude, latitude, idToko, createdAt) VALUES (:detailAlamat, :provinsi, :kabupaten_kota, :kecamatan, :kodepos, :long, :lat, :idToko, :createdAt);
	`

	QueryCreateBerkas = `
		INSERT INTO berkas (idNasabah, SIUP, TDP, NPWP, berkasPendukung, createdAt) VALUES (:idNasabah, :SIUP, :TDP, :NPWP, :berkasPendukung, :createdAt)
	`

	QueryCreateRekening = `
		INSERT INTO rekeningNasabah (noRekening, namaPemilikRekening, namaBank, idNasabah, createdAt) VALUES (:noRekening, :namaPemilikRekening, :namaBank, :idNasabah, :createdAt)
	`

	QueryFindNasabah = `
		SELECT id FROM nasabah
	`

	QueryGetNasabah = `
		SELECT namaLengkap, username, noTlp, email, rekeningNasabah, foto, noKTP, noWA, password FROM nasabah
	`

	QueryGetAllNasabah = `
		SELECT id, username, email, namaLengkap, noKTP, noTlp, noWA, rekeningNasabah, status FROM nasabah
	`

	QueryGetNasabahById = `
		SELECT id, username, email, namaLengkap, noKTP, noTlp, noWA, rekeningNasabah, status, password FROM nasabah WHERE id = :id
	`

	QueryGetAccountInfo = `
		SELECT noRekening, namaPemilikRekening, namaBank FROM rekeningNasabah
	`

	QueryGetUserFile = `
		SELECT SIUP, TDP, NPWP, berkasPendukung FROM berkas
	`

	QueryGetUserBusiness = `
		SELECT id, idNasabah, namaToko, deskripsiToko, jenisUsaha, rantaiUsaha, jumlahKaryawan, omsetPerbulan, fotoProfileToko, website, facebook, instagram, shopee, tokopedia, bukalapak FROM tokoNasabah
	`

	QueryGetBusinessId = `
		SELECT id, idNasabah FROM tokoNasabah
	`

	QueryUpdateBusiness = `
		UPDATE tokoNasabah SET namaToko=:namaToko, deskripsiToko=:deskripsiToko, jenisUsaha=:jenisUsaha, rantaiUsaha=:rantaiUsaha, jumlahKaryawan=:jumlahKaryawan, omsetPerbulan=:omsetPerbulan, fotoProfileToko=:fotoProfileToko, website=:website, facebook=:facebook, instagram=:instagram, shopee=:shopee, tokopedia=:tokopedia, bukalapak=:bukalapak WHERE id=:id
	`

	QueryUpdateBusinessByEmail = `
		UPDATE tokoNasabah SET website=:website, facebook=:facebook, instagram=:instagram WHERE idNasabah=:idNasabah
	`

	QueryGetBusinessSocmed = `
		SELECT id, url, nama, idToko FROM sosialMedia
	`

	QueryUpdateNasabah = `
		UPDATE nasabah SET username=:username, email=:email, namaLengkap=:namaLengkap, noTlp=:noTlp, noKTP=:noKTP, noWA=:noWA, foto=:foto, updateAt=:updateAt WHERE id = :id
	`

	QueryUpdateAccountInfo = `
		UPDATE rekeningNasabah SET noRekening=:noRekening, namaPemilikRekening=:namaPemilikRekening, namaBank=:namaBank, updatedAt=:updatedAt WHERE idNasabah = :idNasabah
	`

	QueryUpdatePassword = `
		UPDATE nasabah SET password=:password, updateAt=:updateAt WHERE id = :id
	`

	QueryCreateSosmed = `
		INSERT INTO sosialMedia (url, nama, idToko) VALUES (:url, :nama, :idToko)
	`

	QueryGetPovince = `
		SELECT id, nama FROM provinsi 
	`

	QueryGetCity = `
		SELECT id, nama, idProvinsi FROM kabupaten_kota
	`

	QueryGetDisctrict = `
		SELECT id, nama FROM kecamatan
	`

	//admin
	QueryCreateAdmin = `
		INSERT INTO admin (usernameAdmin, emailAdmin, namaLengkapAdmin, password, is_active, token, role) VALUES (:usernameAdmin, :emailAdmin, :namaLengkapAdmin, :password, :is_active, :token, :role);
	`

	QueryGetAdminInfo = `
		SELECT id, usernameAdmin, emailAdmin, namaLengkapAdmin, password, is_active, token, role FROM admin
	`

	QueryUpdateStatus = `
		UPDATE admin SET is_active=:is_active WHERE token = :token
	`

	QueryUpdateStatusUser = `
		UPDATE nasabah SET status=:status WHERE id = :id
	`

	QueryGetNasabahAndBusiness = `
		SELECT tokoNasabah.id AS id, nasabah.id AS id, nasabah.namaLengkap AS name, tokoNasabah.namaToko AS business_name,IF(tokoNasabah.jenisUsaha != "",tokoNasabah.jenisUsaha,"") AS business_type,IF(tokoNasabah.rantaiUsaha != "",tokoNasabah.rantaiUsaha,"") AS business_chain,IF(provinsi.nama != "",provinsi.nama,"") AS provinsi, alamatToko.latitude as latitude, alamatToko.longitude as longitude FROM nasabah JOIN tokoNasabah ON nasabah.id=tokoNasabah.idNasabah JOIN alamatToko ON tokoNasabah.id=alamatToko.idToko LEFT JOIN provinsi ON alamatToko.provinsi=provinsi.id
	`

	QueryGetNasabahDetailAddress = `
		SELECT detailAlamat, provinsi, kabupaten_kota, kecamatan, kodepos, IF (provinsi.nama !="",provinsi.nama,"") AS province_text, IF (kabupaten_kota.nama !="",kabupaten_kota.nama,"") AS city_district_text, IF (kecamatan.nama !="",kecamatan.nama,"") AS district_text, longitude AS 'long', latitude AS lat FROM alamatToko LEFT JOIN provinsi ON alamatToko.provinsi=provinsi.id LEFT JOIN kabupaten_kota ON alamatToko.kabupaten_kota=kabupaten_kota.id LEFT JOIN kecamatan ON alamatToko.kecamatan=kecamatan.id
	`

	QueryCountNasabah = `
		SELECT count(1) FROM nasabah
	`

	QueryCountAdmin = `
		SELECT count(1) FROM admin
	`

	//
	QueryGetUserNearby = `
		SELECT nasabah.id as idNasabah, nasabah.namaLengkap as namaLengkap, tokoNasabah.namaToko as namaToko, IF(tokoNasabah.jenisUsaha != "",tokoNasabah.jenisUsaha,"") as jenisUsaha, alamatToko.detailAlamat as detailAlamat, alamatToko.latitude as latitude, alamatToko.longitude as longitude, SQRT(POW(69.1*(latitude-:latitude),2)+POW(69.1*(:longitude-longitude)*COS(latitude/57.3),2)) AS distance FROM alamatToko JOIN tokoNasabah ON alamatToko.idToko=tokoNasabah.id JOIN nasabah ON tokoNasabah.idNasabah=nasabah.id
	`

	QueryGetLocationByPostcode = `
		SELECT kecamatan, kabupaten, provinsi FROM tbl_kodepos
	`

	QueryGetLocationByDsitrict = `
		SELECT idKec,idKota,idProvinsi FROM kecamatan JOIN kabupaten_kota ON kecamatan.idKota = kabupaten_kota.id
	`

	QueryGetSupplierBussiness = `
		SELECT id, idNasabah, namaToko, deskripsiToko FROM tokoNasabah
	`

	QueryCreateRecommendation = `
		INSERT INTO rekomendasi (idNasabah, idSupplier) VALUES (:idNasabah, :idSupplier)
	`

	QueryGetRecommendation = `
		SELECT idSupplier FROM rekomendasi
	`

	//
	QueryCreateProduk = `
		INSERT INTO produk (nama, deskripsi, kategori, harga, stok, satuan, foto, idToko, createdAt) VALUES (:nama, :deskripsi, :kategori, :harga, :stok, :satuan, :foto, :idToko, :createdAt);
	`

	// Data Dashboard
	QueryTotalUser = `
		SELECT COUNT(nasabah.id) as total, MONTH(createdAt) as month, YEAR(createdAt) as year FROM nasabah GROUP BY YEAR(createdAt), MONTH(createdAt)
	`

	QueryTotalStore = `
		SELECT
			COUNT( tokoNasabah.id ) AS total,
			MONTH (IF(tokoNasabah.createdAt = null,tokoNasabah.createdAt,nasabah.createdAt)  ) AS month,
			YEAR ( IF(tokoNasabah.createdAt = null,tokoNasabah.createdAt,nasabah.createdAt)  ) AS year 
		FROM
			tokoNasabah
			JOIN nasabah ON tokoNasabah.idNasabah = nasabah.id 
		GROUP BY
			YEAR (IF(tokoNasabah.createdAt = null,tokoNasabah.createdAt,nasabah.createdAt)),
			MONTH (IF(tokoNasabah.createdAt = null,tokoNasabah.createdAt,nasabah.createdAt))
	`

	QueryTotalProduct = `
		SELECT
			COUNT( produk.id ) AS total,
			MONTH ( createdAt ) AS month,
			YEAR ( createdAt ) AS year 
		FROM
			produk 
		GROUP BY
			YEAR ( createdAt ),
			MONTH (
			createdAt)
	`

	QueryGetAllAdmin = `
		SELECT id, usernameAdmin, emailAdmin, namaLengkapAdmin, is_active, role FROM admin
			
	`

	QueryUpdateStatusAdmin = `
		UPDATE admin SET is_active=:is_active WHERE id = :id
	`

	QueryDeleteRecommend = `
		DELETE FROM rekomendasi WHERE idNasabah = :idNasabah
	`

	QueryDeleteNasabah = `
		DELETE FROM nasabah WHERE id = :id
	`

	QueryDeleteRekeningNasabbah = `
		DELETE FROM rekeningNasabah WHERE idNasabah = :id
	`

	QueryDeleteTokoNasabah = `
		DELETE FROM tokoNasabah WHERE idNasabah = :id
	`

	QueryDeleteBerkas = `
		DELETE FROM berkas WHERE idNasabah = :id
	`

	QueryDeleteAlamatToko = `
		DELETE FROM alamatToko WHERE idToko = :idToko
	`

	QueryDeleteProduk = `
		DELETE FROM produk WHERE idToko = :idToko
	`
)
