package mysql

import (
	"database/sql"
	"time"
)

type UserData struct {
	Fullname      string     `json:"fullname" db:"namaLengkap"`
	Username      string     `json:"username" db:"username"`
	PhoneNumber   string     `json:"phone_number" db:"noTlp"`
	IdNumber      int64      `json:"id_number" db:"noKTP"`
	WANumber      string     `json:"whatsapp_number" db:"noWA"`
	Email         string     `json:"email" db:"email"`
	AccountNumber string     `json:"account_number" db:"rekeningNasabah"`
	Password      string     `json:"password" db:"password"`
	Status        int        `json:"status" db:"status"`
	CreatedAt     *time.Time `json:"createdAt" db:"createdAt"`
	UpdateAt      *time.Time `json:"updateAt" db:"updateAt"`
	Timezone      string     `json:"timezone" db:"timezone"`
	Token         string     `json:"token" db:"token"`
	Foto          string     `json:"foto" db:"foto"`
}

type BusinessAddressData struct {
	BusinessID       int        `json:"business_id" db:"idToko"`
	Address          string     `json:"address" db:"detailAlamat"`
	Province         int        `json:"province" db:"provinsi"`
	CityDistirct     int        `json:"city_distirct" db:"kabupaten_kota"`
	District         int        `json:"district" db:"kecamatan"`
	Postcode         int        `json:"postcode" db:"kodepos"`
	Long             float64    `json:"long" db:"long"`
	Lat              float64    `json:"lat" db:"lat"`
	ProvinceText     string     `json:"province_text" db:"province_text"`
	CityDistirctText string     `json:"city_district_text" db:"city_district_text"`
	DistrictText     string     `json:"district_text" db:"district_text"`
	CreatedAt        *time.Time `json:"createdAt" db:"createdAt"`
	UpdateAt         *time.Time `json:"updateAt" db:"updateAt"`
}

type BusinessProfileData struct {
	ID                  int32      `json:"id" db:"id"`
	UserID              int32      `json:"user_id" db:"idNasabah"`
	BusinessName        string     `json:"business_name" db:"namaToko"`
	BusinessDescription string     `json:"business_description" db:"deskripsiToko"`
	BusinessType        string     `json:"business_type" db:"jenisUsaha"`
	BusinessChain       string     `json:"business_chain" db:"rantaiUsaha"`
	TotalEmployee       int32      `json:"total_employee" db:"jumlahKaryawan"`
	MonthlyIncome       int32      `json:"monthly_income" db:"omsetPerbulan"`
	BusinessPicture     string     `json:"business_picture" db:"fotoProfileToko"`
	Website             string     `json:"website" db:"website"`
	Facebook            string     `json:"facebook" db:"facebook"`
	Instagram           string     `json:"instagram" db:"instagram"`
	Shopee              string     `json:"shopee" db:"shopee"`
	Tokopedia           string     `json:"tokopedia" db:"tokopedia"`
	Bukalapak           string     `json:"bukalapak" db:"bukalapak"`
	CreatedAt           *time.Time `json:"createdAt" db:"createdAt"`
	UpdateAt            *time.Time `json:"updateAt" db:"updateAt"`
}

type UserFile struct {
	UserID      int        `json:"user_id" db:"idNasabah"`
	Siup        string     `json:"siup" db:"SIUP"`
	Tdp         string     `json:"tdp" db:"TDP"`
	Npwp        string     `json:"npwp" db:"NPWP"`
	SupportFile string     `json:"support_file" db:"berkasPendukung"`
	CreatedAt   *time.Time `json:"createdAt" db:"createdAt"`
	UpdateAt    *time.Time `json:"updateAt" db:"updateAt"`
}

type UserAccount struct {
	UserID        int        `json:"user_id" db:"idNasabah"`
	AccountNumber int        `json:"account_number" db:"noRekening"`
	AccountName   string     `json:"account_name" db:"namaPemilikRekening"`
	BankAccount   string     `json:"bank_account" db:"namaBank"`
	CreatedAt     *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt     *time.Time `json:"updatedAt" db:"updatedAt"`
}

type UserId struct {
	ID int `json:"id" db:"id"`
}

type DetailData struct {
	Fullname      string         `json:"fullname" db:"namaLengkap"`
	WANumber      string         `json:"whatsapp_number" db:"noWA"`
	IdNumber      int64          `json:"id_number" db:"noKTP"`
	Username      string         `json:"username" db:"username"`
	PhoneNumber   string         `json:"phone_number" db:"noTlp"`
	Email         string         `json:"email" db:"email"`
	AccountNumber string         `json:"account_number" db:"rekeningNasabah"`
	Foto          sql.NullString `json:"foto" db:"foto"`
	Password      string         `json:"password" db:"password"`
}

type UpdateUser struct {
	ID          int        `json:"id" db:"id"`
	Email       string     `json:"email" db:"email"`
	Fullname    string     `json:"fullname" db:"namaLengkap"`
	Username    string     `json:"username" db:"username"`
	PhoneNumber string     `json:"phone_number" db:"noTlp"`
	Foto        string     `json:"foto" db:"foto"`
	Status      int        `json:"status" db:"status"`
	IdNumber    int64      `json:"id_number" db:"noKTP"`
	WANumber    string     `json:"whatsapp_number" db:"noWA"`
	UpdateAt    *time.Time `json:"updateAt" db:"updateAt"`
}

type UpdateUserPassword struct {
	UserID   int        `json:"user_id" db:"id"`
	Password string     `json:"password" db:"password"`
	UpdateAt *time.Time `json:"updateAt" db:"updateAt"`
}

type AdminData struct {
	ID       int    `json:"id" db:"id"`
	Username string `json:"username" db:"usernameAdmin"`
	Email    string `json:"email" db:"emailAdmin"`
	FullName string `json:"full_name" db:"namaLengkapAdmin"`
	Password string `json:"password" db:"password"`
	Token    string `json:"token" db:"token"`
	IsActive int    `json:"is_active" db:"is_active"`
	Role     int    `json:"role" db:"role"`
}

type UserAndBusiness struct {
	ID            int     `json:"id" db:"id"`
	Name          string  `json:"name" db:"name"`
	BusinessName  string  `json:"business_name" db:"business_name"`
	BusinessType  string  `json:"business_type" db:"business_type"`
	BusinessChain string  `json:"business_chain" db:"business_chain"`
	Province      string  `json:"province" db:"provinsi"`
	Long          float64 `json:"long" db:"longitude"`
	Lat           float64 `json:"lat" db:"latitude"`
}

type BusinessSocmed struct {
	ID         int    `json:"id" db:"id"`
	URL        string `json:"url" db:"url"`
	Name       string `json:"name" db:"nama"`
	BusinessID int    `json:"business_id" db:"idToko"`
}

type Province struct {
	ID         int    `json:"id" db:"id"`
	Name       string `json:"name" db:"nama"`
	ProvinceId int    `json:"province_id" db:"idProvinsi"`
	CityId     int    `json:"city_id" db:"idKota"`
}

type City struct {
	ID         int    `json:"id" db:"id"`
	Name       string `json:"name" db:"nama"`
	ProvinceId int    `json:"province_id" db:"idProvinsi"`
}

type District struct {
	ID     int    `json:"id" db:"id"`
	Name   string `json:"name" db:"nama"`
	CityId int    `json:"city_id" db:"idKota"`
}

type UserRecommendation struct {
	Id           int             `json:"id" db:"id"`
	UserID       int             `json:"user_id" db:"idNasabah"`
	Fullname     string          `json:"fullname" db:"namaLengkap"`
	BusinessName string          `json:"business_name" db:"namaToko"`
	BusinessType string          `json:"business_type" db:"jenisUsaha"`
	Address      string          `json:"address" db:"detailAlamat"`
	Long         float64         `json:"long" db:"longitude"`
	Lat          float64         `json:"lat" db:"latitude"`
	Distance     float64         `json:"distance" db:"distance"`
	City         string          `json:"city" db:"kota"`
	Image        sql.NullString  `json:"image" db:"foto"`
	SupplierName sql.NullString  `json:"supplier_name" db:"deskripsi"`
	Price        sql.NullFloat64 `json:"price" db:"price"`
	ProductName  sql.NullString  `json:"product_name" db:"namaProduk"`
}

type LocationName struct {
	Province string `json:"province" db:"provinsi"`
	City     string `json:"city" db:"kabupaten"`
	District string `json:"district" db:"kecamatan"`
}

type LocationId struct {
	ProvinceId int32 `json:"province_id" db:"idProvinsi"`
	CityId     int32 `json:"city_id" db:"idKota"`
	DistrictId int32 `json:"district_id" db:"idKec"`
}

type Location struct {
	ID         int    `json:"id" db:"id"`
	Name       string `json:"name" db:"nama"`
	ProvinceId int    `json:"province_id" db:"idProvinsi"`
}

type SupportSupplier struct {
	ID           int32          `json:"id" db:"id"`
	UserId       int32          `json:"user_id" db:"idNasabah"`
	BusinessName string         `json:"business_name" db:"namaToko"`
	Description  sql.NullString `json:"description,omitempty" db:"deskripsiToko"`
}

type SupplierRecommend struct {
	UserId     int32 `json:"user_id" db:"idNasabah"`
	SupplierId int32 `json:"supplier_id" db:"idSupplier"`
}

type Product struct {
	ID          int32      `json:"id" db:"id"`
	Nama        string     `json:"nama" db:"nama"`
	Description string     `json:"description" db:"deskripsi"`
	Category    string     `json:"category" db:"kategori"`
	Price       float32    `json:"price" db:"harga"`
	Stock       int        `json:"stock" db:"stok"`
	Unit        string     `json:"unit" db:"satuan"`
	Image       string     `json:"image" db:"foto"`
	BusinessId  int32      `json:"business_id" db:"idToko"`
	CreatedAt   *time.Time `json:"createdAt" db:"createdAt"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"updatedAt"`
}

type DataDashboard struct {
	Total float64 `json:"total" db:"total"`
	Month int32   `json:"month" db:"month"`
	Year  int32   `json:"year" db:"year"`
}
