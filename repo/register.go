package repo

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo/mysql"
	er "gitlab.com/erwinnekketsu/embrio-service-user.git/util/errors"
)

type Register struct {
	builderReg *builder.User
	iMysql     mysql.IMysql
	iAdmin     IAdmin
	embrioDB   *sqlx.DB
}

func NewRegister() *Register {
	return &Register{
		builderReg: builder.NewUser(),
		iMysql:     mysql.NewClient(),
		iAdmin:     NewAdmin(),
		embrioDB:   mysql.EmbrioDB,
	}
}

func (r *Register) Register(ctx context.Context, data entity.Register) (s string, err error) {
	reqUserData, token := r.builderReg.UserDataRequest(data)
	lastId, err := r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqUserData, mysql.QueryCreateNasabah)
	if err != nil {
		return "", err
	}
	reqUserBankAccount := r.builderReg.UserAccountRequest(int(lastId), data.UserData)
	_, err = r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqUserBankAccount, mysql.QueryCreateRekening)
	if err != nil {
		return "", err
	}
	reqBusinessData := r.builderReg.BusinessDataRequest(int(lastId), data)
	businessId, err := r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqBusinessData, mysql.QueryCreateTokoNasabah)
	if err != nil {
		return "", err
	}
	reqBusinessAddressData := r.builderReg.BusinessAddressRequest(int(businessId), data)
	_, err = r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqBusinessAddressData, mysql.QueryCreateAlamatTokoNasabah)
	if err != nil {
		return "", err
	}
	go func() {
		ctx = context.Background()
		reqBusinessUserFile := r.builderReg.UserFileRequest(ctx, int(lastId), data.UserFile)
		_, err = r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqBusinessUserFile, mysql.QueryCreateBerkas)
		if err != nil {
			return
		}
	}()
	return token, nil
}

func (r *Register) RegisterAdmin(ctx context.Context, data entity.AdminData) (token string, err error) {
	checkEmail, err := r.iAdmin.GetAdminByEmail(ctx, data.Email)
	if checkEmail.ID != 0 {
		return "", er.ErrBadRequest("email already exist")
	}
	checkUsername, err := r.iAdmin.GetAdminByUsername(ctx, data.Username)
	if checkUsername.ID != 0 {
		return "", er.ErrBadRequest("username already exist")
	}
	reqAdminData, token := r.builderReg.RegisterAdminRequest(data)
	_, err = r.iMysql.CreateOrUpdate(ctx, r.embrioDB, reqAdminData, mysql.QueryCreateAdmin)
	if err != nil {
		return "", err
	}
	// go func() {
	// 	util.SendMail(data.Email, token)
	// }()
	return token, nil
}
