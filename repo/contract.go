package repo

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
)

type i map[string]interface{}

type IRegister interface {
	Register(ctx context.Context, data entity.Register) (s string, err error)
	RegisterAdmin(ctx context.Context, data entity.AdminData) (token string, err error)
}

type IUser interface {
	GetUserByEmail(ctx context.Context, email string) (data mysql.UserId, err error)
	GetUserByRekening(ctx context.Context, noRekening string) (data mysql.UserId, err error)
	GetUserByNoTlp(ctx context.Context, noTlp string) (data mysql.UserId, err error)
	GetUserById(ctx context.Context, Id int) (data mysql.DetailData, err error)
	GetUserAccountInfo(ctx context.Context, Id int) (data mysql.UserAccount, err error)
	GetUserFile(ctx context.Context, Id int) (data mysql.UserFile, err error)
	UpdateUser(ctx context.Context, data entity.UpdateUser) error
	UpdateAccountInfo(ctx context.Context, data entity.UpdateAccountInfo) error
	UpdateUserPassword(ctx context.Context, req entity.UpdateUserPassword) error
	UpdateUserStatus(ctx context.Context, ID int64) error
	GetUserUserAndBusiness(ctx context.Context, page int, limit int, status int) (data []entity.UserAndBusiness, pagintaion *util.Pagination, err error)
	excel(ctx context.Context)
	GetLocation(ctx context.Context, address string) (data entity.Geolocation)
	GetLoc(ctx context.Context, name string, tag string) (data mysql.Province, err error)
	GetLocById(ctx context.Context, id int32, tag string) (data []entity.Location, err error)
	GetUserUserAndBusinessById(ctx context.Context, id int) (data mysql.UserAndBusiness, err error)
	GetUserRecommendation(ctx context.Context, id int, tag string) (data []mysql.UserRecommendation, err error)
	GetLocationByPostcode(ctx context.Context, postcode int32) (data mysql.LocationName, err error)
	GetLocationId(ctx context.Context, postcode int32) (data entity.LocationId, err error)
	GetSupplierList(ctx context.Context, query string) (data []mysql.SupportSupplier, err error)
	CreateSupplierRecommend(ctx context.Context, data entity.SupplierRecommend) error
	GetIdsRecommendation(ctx context.Context, id int) (ids []int, err error)
	GetBusinessById(ctx context.Context, user_id int32, id int32) (data mysql.BusinessProfileData, err error)
	UpdateBusiness(ctx context.Context, data entity.BusinessProfileData) error
	GetDataDashboard(ctx context.Context, querySort string) (data entity.DataDashboard, err error)
	DeleteSupplierRecommend(ctx context.Context, user_id int32) error
	DetailFullUser(ctx context.Context, id int32) (data entity.DetailData, err error)
	DeleteUser(ctx context.Context, id int32) error
}

type IAdmin interface {
	GetAdminByEmail(ctx context.Context, email string) (data mysql.AdminData, err error)
	GetAdminByUsername(ctx context.Context, username string) (data mysql.AdminData, err error)
	CheckAdminToken(ctx context.Context, token string) (data mysql.AdminData, err error)
	AdminVerify(ctx context.Context, token string) error
	GetAllAdmin(ctx context.Context, page int, limit int, status int) (data []entity.AdminData, pagintaion *util.Pagination, err error)
	UpdateAdminStatus(ctx context.Context, Id int32, adminId int32, status int32) error
}
