package repo

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo/mysql"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
	er "gitlab.com/erwinnekketsu/embrio-service-user.git/util/errors"
)

type Admin struct {
	builderReg *builder.User
	iMysql     mysql.IMysql
	embrioDB   *sqlx.DB
}

func NewAdmin() *Admin {
	return &Admin{
		builderReg: builder.NewUser(),
		iMysql:     mysql.NewClient(),
		embrioDB:   mysql.EmbrioDB,
	}
}

func (a *Admin) GetAdminByEmail(ctx context.Context, email string) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"emailAdmin$eq!": email,
		},
	}
	err = a.iMysql.Get(
		ctx,
		a.embrioDB,
		&data,
		query,
		mysql.QueryGetAdminInfo,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (a *Admin) GetAdminByUsername(ctx context.Context, username string) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"usernameAdmin$eq!": username,
		},
	}
	err = a.iMysql.Get(
		ctx,
		a.embrioDB,
		&data,
		query,
		mysql.QueryGetAdminInfo,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (a *Admin) CheckAdminToken(ctx context.Context, token string) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"token$eq!": token,
		},
	}
	err = a.iMysql.Get(
		ctx,
		a.embrioDB,
		&data,
		query,
		mysql.QueryGetAdminInfo,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (a *Admin) AdminVerify(ctx context.Context, token string) error {
	checkToken, err := a.CheckAdminToken(ctx, token)
	if checkToken.ID == 0 {
		return er.ErrBadRequest("token not valid")
	}
	reqVerify := a.builderReg.AdminVerify(token)
	_, err = a.iMysql.CreateOrUpdate(ctx, a.embrioDB, reqVerify, mysql.QueryUpdateStatus)
	if err != nil {
		return err
	}
	return nil
}

func (a *Admin) UpdateAdminStatus(ctx context.Context, Id int32, adminId int32, status int32) error {
	checkAdminRole, err := a.GetAdminById(ctx, adminId)
	if checkAdminRole.Role == 0 {
		return er.ErrBadRequest("this admin cannot change status")
	}
	reqVerify := a.builderReg.AdminUpdateStatusRequest(Id, status)
	_, err = a.iMysql.CreateOrUpdate(ctx, a.embrioDB, reqVerify, mysql.QueryUpdateStatusAdmin)
	if err != nil {
		return err
	}
	return nil
}

func (a *Admin) GetAdminById(ctx context.Context, id int32) (data mysql.AdminData, err error) {
	query := &util.Query{
		Filter: map[string]interface{}{
			"id$eq!": id,
		},
	}
	err = a.iMysql.Get(
		ctx,
		a.embrioDB,
		&data,
		query,
		mysql.QueryGetAdminInfo,
	)

	if err != nil {
		return data, err
	}
	return data, nil
}

func (a *Admin) GetAllAdmin(ctx context.Context, page int, limit int, status int) (data []entity.AdminData, pagintaion *util.Pagination, err error) {
	queryAdmin := &util.Query{
		Filter: map[string]interface{}{
			"role$eq!": status,
		},
	}
	datas, paginations, err := a.iMysql.FindWithPaginationAdmin(
		ctx,
		a.embrioDB,
		queryAdmin,
		util.NewPagination(int32(page), int32(limit)),
		mysql.QueryGetAllAdmin,
	)

	log.Println(datas)
	if err != nil {
		log.Println(err)
		return data, &util.Pagination{}, err
	}

	for _, v := range datas {
		dataAdmin := entity.AdminData{
			ID:       v.ID,
			Username: v.Username,
			Email:    v.Email,
			FullName: v.FullName,
			Status:   v.IsActive,
			Role:     v.Role,
		}
		data = append(data, dataAdmin)
	}
	return data, paginations, nil
}
