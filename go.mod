module gitlab.com/erwinnekketsu/embrio-service-user.git

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/cloudinary/cloudinary-go v1.2.0
	github.com/go-redis/redis/v8 v8.11.0
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	go.elastic.co/apm/module/apmgrpc v1.12.0
	go.elastic.co/apm/module/apmsql v1.12.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/grpc v1.39.0
	google.golang.org/grpc/examples v0.0.0-20210708170655-30dfb4b933a5 // indirect
	google.golang.org/protobuf v1.25.0
)
