package grpc

import (
	"context"
	"encoding/json"
	"log"

	action "gitlab.com/erwinnekketsu/embrio-service-user.git/action"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type GrpcServer struct {
	builder *builder.Grpc
}

func (gs *GrpcServer) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	var resp pb.RegisterResponse
	token, err := action.NewRegister().Handler(ctx, req)
	resp.Token = token
	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) DetailUser(ctx context.Context, req *pb.DetailUserRequest) (*pb.DetailUserResponse, error) {
	var resp pb.DetailUserResponse
	detailUser, err := action.NewDetailUser().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetDetailUserResponse(detailUser), nil
}

func (gs *GrpcServer) AccountInfo(ctx context.Context, req *pb.AccountInfoRequest) (*pb.AccountInfoResponse, error) {
	var resp pb.AccountInfoResponse
	AccountInfo, err := action.NewAccountInfo().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetAccountResponse(AccountInfo), nil
}

func (gs *GrpcServer) UserFile(ctx context.Context, req *pb.UserFileRequest) (*pb.UserFileResponse, error) {
	var resp pb.UserFileResponse
	userFile, err := action.NewUserFile().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetUserFileResponse(userFile), nil
}

func (gs *GrpcServer) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateUser().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateAccountInfo(ctx context.Context, req *pb.UpdateAccountInfoRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateAccountInfo().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UpdateUserPassword(ctx context.Context, req *pb.UpdateUserPasswordRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateUserPassword().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) RegisterAdmin(ctx context.Context, req *pb.RegisterAdminRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewRegisterAdmin().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) AdminVerification(ctx context.Context, req *pb.AdminVerificationRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewAdminVerify().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) UserVerification(ctx context.Context, req *pb.UserVerificationRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateUserStatus().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) ListNasabah(ctx context.Context, req *pb.ListNasabahRequest) (*pb.ListNasabahResponse, error) {
	var resp pb.ListNasabahResponse
	data, pagination, err := action.NewListNasabah().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListNasabahResponse{
		Nasabah:    gs.builder.GetUserAndBusinessResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) UserRecommendation(ctx context.Context, req *pb.UserRecommendationRequest) (*pb.UserRecommendationResponse, error) {
	var resp pb.UserRecommendationResponse
	data, err := action.NewUserRecommendation().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &pb.UserRecommendationResponse{
		UserRecommendation: gs.builder.GetUserRecommend(data),
	}, nil
}

func (gs *GrpcServer) GetLocationCode(ctx context.Context, req *pb.GetLocationCodeRequest) (*pb.GetLocationCodeResponse, error) {
	var resp pb.GetLocationCodeResponse
	getLocationCode, err := action.NewGetLocationCode().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetLocationCodeResponse(getLocationCode), nil
}

func (gs *GrpcServer) LocationList(ctx context.Context, req *pb.LocationListRequest) (*pb.LocationListResponse, error) {
	var resp pb.LocationListResponse
	data, err := action.NewLocationList().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &pb.LocationListResponse{
		Location: gs.builder.GetLocationList(data),
	}, nil
}

func (gs *GrpcServer) SupportSupplierList(ctx context.Context, req *pb.SupportSupplierListRequest) (*pb.SupportSupplierListResponse, error) {
	var resp pb.SupportSupplierListResponse
	data, err := action.NewSupplierSupport().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &pb.SupportSupplierListResponse{
		SupportSupplier: gs.builder.GetSupplierList(data),
	}, nil
}

func (gs *GrpcServer) CreateSupplierRecommend(ctx context.Context, req *pb.CreateSupplierRecommendRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewCreateRecommend().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) DetailBusiness(ctx context.Context, req *pb.DetailBusinessRequest) (*pb.DetailBusinessResponse, error) {
	var resp pb.DetailBusinessResponse
	detailUser, err := action.NewDetailBusiness().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetDetailBusinessResponse(detailUser), nil
}

func (gs *GrpcServer) UpdateDetailBusiness(ctx context.Context, req *pb.UpdateDetailBusinessRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateBusiness().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) DashboardData(ctx context.Context, req *pb.DashboardDataRequest) (*pb.DashboardDataResponse, error) {
	var resp *pb.DashboardDataResponse
	data, err := action.NewDataDashboard().Handler(ctx, req)

	if err != nil {
		return resp, err
	}
	bytes, _ := json.Marshal(data)
	_ = json.Unmarshal(bytes, &resp)
	log.Println(resp)
	return resp, nil
}

func (gs *GrpcServer) ListAdmin(ctx context.Context, req *pb.ListAdminRequest) (*pb.ListAdminResponse, error) {
	var resp pb.ListAdminResponse
	data, pagination, err := action.NewListAdmin().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	resPagination := &pb.Pagination{
		PageSize:     int64(pagination.PageSize),
		CurrentPage:  int64(pagination.CurrentPage),
		TotalPage:    int64(pagination.TotalPage),
		TotalResults: int64(pagination.TotalResult),
	}
	return &pb.ListAdminResponse{
		Admin:      gs.builder.GetUserAdminResponse(data),
		Pagination: resPagination,
	}, nil
}

func (gs *GrpcServer) ChangeStatusAdmin(ctx context.Context, req *pb.ChangeStatusAdminRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewUpdateAdminStatus().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func (gs *GrpcServer) DetailUserFull(ctx context.Context, req *pb.DetailUserRequest) (*pb.DetailUserFullResponse, error) {
	var resp pb.DetailUserFullResponse
	detailUser, err := action.NewDetaiFulllUser().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return gs.builder.GetDetailFullUserResponse(detailUser), nil
}

func (gs *GrpcServer) DeleteUserRegister(ctx context.Context, req *pb.DetailUserRequest) (*pb.NoResponse, error) {
	var resp pb.NoResponse
	err := action.NewDeleteUser().Handler(ctx, req)

	if err != nil {
		return &resp, err
	}
	return &resp, nil
}

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		builder: builder.NewGrpc(),
	}
}
