package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type LocationList struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewLocationList() *LocationList {
	return &LocationList{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (h *LocationList) Handler(ctx context.Context, req *pb.LocationListRequest) (data []entity.Location, err error) {
	return h.repoUser.GetLocById(ctx, req.Id, req.Tag)
}
