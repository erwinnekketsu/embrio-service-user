package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateAdminStatus struct {
	builder   *builder.Grpc
	repoAdmin repo.IAdmin
}

func NewUpdateAdminStatus() *UpdateAdminStatus {
	return &UpdateAdminStatus{
		builder:   builder.NewGrpc(),
		repoAdmin: repo.NewAdmin(),
	}
}

func (u *UpdateAdminStatus) Handler(ctx context.Context, req *pb.ChangeStatusAdminRequest) error {
	err := u.repoAdmin.UpdateAdminStatus(ctx, req.Id, req.AdminId, req.Status)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
