package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
)

type ListAdmin struct {
	builder   *builder.Grpc
	repoAdmin repo.IAdmin
}

func NewListAdmin() *ListAdmin {
	return &ListAdmin{
		builder:   builder.NewGrpc(),
		repoAdmin: repo.NewAdmin(),
	}
}

func (h *ListAdmin) Handler(ctx context.Context, req *pb.ListAdminRequest) (data []entity.AdminData, pagintaion *util.Pagination, err error) {
	return h.repoAdmin.GetAllAdmin(ctx, int(req.Page), int(req.Limit), int(req.Status))
}
