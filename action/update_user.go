package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateUser struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUpdateUser() *UpdateUser {
	return &UpdateUser{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *UpdateUser) Handler(ctx context.Context, req *pb.UpdateUserRequest) error {
	reqUpdateUser := u.builder.UpdateUserRequest(req)
	err := u.repoUser.UpdateUser(ctx, reqUpdateUser)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
