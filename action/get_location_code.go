package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type GetLocationCode struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewGetLocationCode() *GetLocationCode {
	return &GetLocationCode{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (du *GetLocationCode) Handler(ctx context.Context, req *pb.GetLocationCodeRequest) (entity.LocationId, error) {
	var data entity.LocationId
	data, _ = du.repoUser.GetLocationId(ctx, req.Postcode)

	return data, nil
}
