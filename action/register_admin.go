package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type RegisterAdmin struct {
	builder      *builder.Grpc
	repoRegister repo.IRegister
}

func NewRegisterAdmin() *RegisterAdmin {
	return &RegisterAdmin{
		builder:      builder.NewGrpc(),
		repoRegister: repo.NewRegister(),
	}
}

func (ra *RegisterAdmin) Handler(ctx context.Context, req *pb.RegisterAdminRequest) error {
	regRegister := ra.builder.RegisterAdmin(req)
	_, err := ra.repoRegister.RegisterAdmin(ctx, regRegister)
	if err != nil {
		return err
	}
	return nil
}
