package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type DetailBusiness struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewDetailBusiness() *DetailBusiness {
	return &DetailBusiness{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (du *DetailBusiness) Handler(ctx context.Context, req *pb.DetailBusinessRequest) (entity.BusinessProfileData, error) {
	var data entity.BusinessProfileData
	detailBusiness, _ := du.repoUser.GetBusinessById(ctx, req.UserId, req.Id)
	log.Println(detailBusiness)
	data.ID = detailBusiness.ID
	data.UserID = detailBusiness.UserID
	data.BusinessName = detailBusiness.BusinessName
	data.BusinessDescription = detailBusiness.BusinessDescription
	data.BusinessType = detailBusiness.BusinessType
	data.BusinessChain = detailBusiness.BusinessChain
	data.TotalEmployee = detailBusiness.TotalEmployee
	data.MonthlyIncome = detailBusiness.MonthlyIncome
	data.BusinessPicture = detailBusiness.BusinessPicture
	data.Website = detailBusiness.Website
	data.Facebook = detailBusiness.Facebook
	data.Instagram = detailBusiness.Instagram
	data.Shopee = detailBusiness.Shopee
	data.Tokopedia = detailBusiness.Tokopedia
	data.Bukalapak = detailBusiness.Bukalapak

	return data, nil
}
