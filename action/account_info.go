package action

import (
	"context"
	"strconv"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type AccountInfo struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewAccountInfo() *AccountInfo {
	return &AccountInfo{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (ai *AccountInfo) Handler(ctx context.Context, req *pb.AccountInfoRequest) (entity.AccountInfo, error) {
	var data entity.AccountInfo
	detailUser, _ := ai.repoUser.GetUserAccountInfo(ctx, int(req.Id))
	accountNumber := strconv.Itoa(detailUser.AccountNumber)
	data.AccountNumber = accountNumber
	data.AccountName = detailUser.AccountName
	data.BankAccount = detailUser.BankAccount
	return data, nil
}
