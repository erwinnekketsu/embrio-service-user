package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
	er "gitlab.com/erwinnekketsu/embrio-service-user.git/util/errors"
)

type Register struct {
	builder      *builder.Grpc
	repoRegister repo.IRegister
	repoUser     repo.IUser
}

func (h *Register) Handler(ctx context.Context, req *pb.RegisterRequest) (token string, err error) {
	reqBuilder := h.builder.Register(req)
	log.Println("request : ", reqBuilder)
	checkEmail, _ := h.repoUser.GetUserByEmail(ctx, reqBuilder.UserData.Email)
	if checkEmail.ID != 0 {
		return "", er.ErrBadRequest("email already exist")
	}
	checkRekening, _ := h.repoUser.GetUserByRekening(ctx, reqBuilder.UserData.AccountNumber)
	if checkRekening.ID != 0 {
		return "", er.ErrBadRequest("account number already exist")
	}
	checkNoTlp, _ := h.repoUser.GetUserByNoTlp(ctx, reqBuilder.UserData.PhoneNumber)
	if checkNoTlp.ID != 0 {
		return "", er.ErrBadRequest("phone number already exist")
	}
	token, err = h.repoRegister.Register(ctx, reqBuilder)
	if err != nil {
		return "", err
	}
	log.Println("response :", token)
	return token, nil
}

func NewRegister() *Register {
	return &Register{
		builder:      builder.NewGrpc(),
		repoRegister: repo.NewRegister(),
		repoUser:     repo.NewUser(),
	}
}
