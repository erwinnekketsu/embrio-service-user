package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type DetailUser struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewDetailUser() *DetailUser {
	return &DetailUser{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (du *DetailUser) Handler(ctx context.Context, req *pb.DetailUserRequest) (entity.DetailData, error) {
	var data entity.DetailData
	detailUser, _ := du.repoUser.GetUserById(ctx, int(req.Id))
	data.AccountNumber = detailUser.AccountNumber
	data.Email = detailUser.Email
	data.Fullname = detailUser.Fullname
	data.Username = detailUser.Username
	data.IdNumber = detailUser.IdNumber
	data.PhoneNumber = detailUser.PhoneNumber
	data.WANumber = detailUser.WANumber
	data.Foto = detailUser.Foto.String

	return data, nil
}
