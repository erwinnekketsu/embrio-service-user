package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateBusiness struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUpdateBusiness() *UpdateBusiness {
	return &UpdateBusiness{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *UpdateBusiness) Handler(ctx context.Context, req *pb.UpdateDetailBusinessRequest) error {
	accountInfoReq := u.builder.UpdateBusinessRequest(req)
	err := u.repoUser.UpdateBusiness(ctx, accountInfoReq)
	if err != nil {
		return err
	}
	return nil
}
