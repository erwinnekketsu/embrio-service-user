package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type AdminVerify struct {
	builder   *builder.Grpc
	repoAdmin repo.IAdmin
}

func NewAdminVerify() *AdminVerify {
	return &AdminVerify{
		builder:   builder.NewGrpc(),
		repoAdmin: repo.NewAdmin(),
	}
}

func (av *AdminVerify) Handler(ctx context.Context, req *pb.AdminVerificationRequest) error {
	err := av.repoAdmin.AdminVerify(ctx, req.Token)
	if err != nil {
		return err
	}
	return nil
}
