package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type CreateRecommend struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewCreateRecommend() *CreateRecommend {
	return &CreateRecommend{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *CreateRecommend) Handler(ctx context.Context, req *pb.CreateSupplierRecommendRequest) error {
	log.Println(req)
	err := u.repoUser.DeleteSupplierRecommend(ctx, req.Id)
	if err != nil {
		log.Println(err)
		return err
	}
	for i := 0; i < len(req.SupplierId); i++ {
		reqSupp := u.builder.CreateSupplierRecommend(req.Id, req.SupplierId[i])
		err := u.repoUser.CreateSupplierRecommend(ctx, reqSupp)
		if err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}
