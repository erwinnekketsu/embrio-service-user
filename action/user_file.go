package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UserFile struct {
	repoUser repo.IUser
}

func NewUserFile() *UserFile {
	return &UserFile{
		repoUser: repo.NewUser(),
	}
}

func (uf *UserFile) Handler(ctx context.Context, req *pb.UserFileRequest) (data entity.UserFile, err error) {
	userFile, _ := uf.repoUser.GetUserFile(ctx, int(req.Id))
	data.Siup = userFile.Siup
	data.Npwp = userFile.Npwp
	data.Tdp = userFile.Tdp
	data.SupportFile = userFile.SupportFile
	return data, nil
}
