package action

import (
	"context"
	"strconv"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UserRecommendation struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUserRecommendation() *UserRecommendation {
	return &UserRecommendation{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (h *UserRecommendation) Handler(ctx context.Context, req *pb.UserRecommendationRequest) (data []entity.UserRecommendation, err error) {
	userData, _ := h.repoUser.GetUserRecommendation(ctx, int(req.Id), req.Tag)

	for _, v := range userData {
		dst := int64(v.Distance)
		dstText := strconv.Itoa(int(dst)) + " km"
		userRecommend := entity.UserRecommendation{
			Id:           v.Id,
			UserID:       v.UserID,
			Fullname:     v.Fullname,
			BusinessName: v.BusinessName,
			Address:      v.Address,
			Long:         v.Long,
			Lat:          v.Lat,
			Distance:     v.Distance,
			DistanceText: dstText,
			City:         v.City,
			Image:        v.Image.String,
			SupplierName: v.SupplierName.String,
			Price:        v.Price.Float64,
		}
		data = append(data, userRecommend)
	}
	return data, err
}
