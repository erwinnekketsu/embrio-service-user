package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateUserPassword struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUpdateUserPassword() *UpdateUserPassword {
	return &UpdateUserPassword{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *UpdateUserPassword) Handler(ctx context.Context, req *pb.UpdateUserPasswordRequest) error {
	dataReq := u.builder.UpdateUserPasswordRequest(req)
	err := u.repoUser.UpdateUserPassword(ctx, dataReq)
	if err != nil {
		return err
	}
	return nil
}
