package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type DeleteUser struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewDeleteUser() *DeleteUser {
	return &DeleteUser{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (du *DeleteUser) Handler(ctx context.Context, req *pb.DetailUserRequest) error {
	err := du.repoUser.DeleteUser(ctx, req.Id)
	if err != nil {
		return err
	}
	return nil
}
