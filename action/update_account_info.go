package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateAccountInfo struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUpdateAccountInfo() *UpdateAccountInfo {
	return &UpdateAccountInfo{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *UpdateAccountInfo) Handler(ctx context.Context, req *pb.UpdateAccountInfoRequest) error {
	accountInfoReq := u.builder.UpdateAccountInfoRequest(req)
	err := u.repoUser.UpdateAccountInfo(ctx, accountInfoReq)
	if err != nil {
		return err
	}
	return nil
}
