package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/util"
)

type ListNasabah struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewListNasabah() *ListNasabah {
	return &ListNasabah{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (h *ListNasabah) Handler(ctx context.Context, req *pb.ListNasabahRequest) (data []entity.UserAndBusiness, pagintaion *util.Pagination, err error) {
	return h.repoUser.GetUserUserAndBusiness(ctx, int(req.Page), int(req.Limit), int(req.Status))
}
