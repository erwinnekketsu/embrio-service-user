package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type UpdateUserStatus struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewUpdateUserStatus() *UpdateUserStatus {
	return &UpdateUserStatus{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (u *UpdateUserStatus) Handler(ctx context.Context, req *pb.UserVerificationRequest) error {
	err := u.repoUser.UpdateUserStatus(ctx, req.Id)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
