package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type SupplierSupport struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewSupplierSupport() *SupplierSupport {
	return &SupplierSupport{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (h *SupplierSupport) Handler(ctx context.Context, req *pb.SupportSupplierListRequest) (data []entity.SupportSupplier, err error) {
	log.Println(req)
	suppData, err := h.repoUser.GetSupplierList(ctx, req.Query)
	if err != nil {
		return nil, err
	}
	for _, v := range suppData {
		suppRecommend := entity.SupportSupplier{
			ID:           v.ID,
			UserId:       v.UserId,
			BusinessName: v.BusinessName,
			Description:  v.Description.String,
		}
		data = append(data, suppRecommend)
	}
	return data, nil
}
