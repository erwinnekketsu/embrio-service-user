package action

import (
	"context"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type DetaiFulllUser struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewDetaiFulllUser() *DetaiFulllUser {
	return &DetaiFulllUser{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (du *DetaiFulllUser) Handler(ctx context.Context, req *pb.DetailUserRequest) (entity.DetailData, error) {
	var data entity.DetailData
	data, _ = du.repoUser.DetailFullUser(ctx, req.Id)

	return data, nil
}
