package action

import (
	"context"
	"log"

	"gitlab.com/erwinnekketsu/embrio-service-user.git/builder"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/entity"
	"gitlab.com/erwinnekketsu/embrio-service-user.git/repo"
	pb "gitlab.com/erwinnekketsu/embrio-service-user.git/transport/grpc/proto/user"
)

type DataDashboard struct {
	builder  *builder.Grpc
	repoUser repo.IUser
}

func NewDataDashboard() *DataDashboard {
	return &DataDashboard{
		builder:  builder.NewGrpc(),
		repoUser: repo.NewUser(),
	}
}

func (h *DataDashboard) Handler(ctx context.Context, req *pb.DashboardDataRequest) (data entity.DataDashboard, err error) {
	data, err = h.repoUser.GetDataDashboard(ctx, req.Query)
	if err != nil {
		log.Println(err)
	}

	return data, nil
}
