package entity

type DetailData struct {
	Fullname            string              `json:"fullname,omitempty"`
	Username            string              `json:"username,omitempty"`
	PhoneNumber         string              `json:"phone_number,omitempty"`
	Email               string              `json:"email,omitempty"`
	AccountNumber       string              `json:"account_number,omitempty"`
	IdNumber            int64               `json:"id_number,omitempty"`
	WANumber            string              `json:"whatsapp_number,omitempty"`
	Foto                string              `json:"foto,omitempty"`
	UserFile            UserFile            `json:"user_file,omiempty"`
	AccountInfo         AccountInfo         `json:"accountInfo,omiempty"`
	BusinessProfileData BusinessProfileData `json:"detailBusiness,omiempty"`
	BusinessData        BusinessData        `json:"business_data,omiempty"`
}

type UpdateUser struct {
	ID          int    `json:"id" `
	Email       string `json:"email" `
	Fullname    string `json:"fullname" `
	Username    string `json:"username" `
	PhoneNumber string `json:"phone_number" `
	Foto        string `json:"foto" `
	IdNumber    int64  `json:"id_number" `
	WANumber    string `json:"whatsapp_number" `
	Status      int64  `json:"status" `
}

type UpdateUserPassword struct {
	UserID      int    `json:"user_id"`
	Password    string `json:"password"`
	NewPassword string `json:"new_password"`
}

type AdminData struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	FullName string `json:"full_name"`
	Password string `json:"password"`
	Token    string `json:"token"`
	IsActive int    `json:"is_active"`
	Role     int    `json:"role"`
	Status   int    `json:"status"`
}

type UserAndBusiness struct {
	ID            int    `json:"id"`
	Name          string `json:"name"`
	BusinessName  string `json:"business_name"`
	BusinessType  string `json:"business_type"`
	BusinessChain string `json:"business_chain"`
	Province      string `json:"province"`
}

type Geolocation struct {
	Response struct {
		MetaInfo struct {
			Timestamp string `json:"Timestamp"`
		} `json:"MetaInfo"`
		View []struct {
			Type   string `json:"_type"`
			ViewID int    `json:"ViewId"`
			Result []struct {
				MatchType string `json:"MatchType"`
				Location  struct {
					DisplayPosition struct {
						Latitude  float64 `json:"Latitude"`
						Longitude float64 `json:"Longitude"`
					} `json:"DisplayPosition"`
					Address struct {
						Label          string `json:"Label"`
						Country        string `json:"Country"`
						County         string `json:"County"`
						City           string `json:"City"`
						District       string `json:"District"`
						Subdistrict    string `json:"Subdistrict"`
						Street         string `json:"Street"`
						HouseNumber    string `json:"HouseNumber"`
						PostalCode     string `json:"PostalCode"`
						AdditionalData []struct {
							Value string `json:"value"`
							Key   string `json:"key"`
						} `json:"AdditionalData"`
					} `json:"Address"`
				} `json:"Location"`
			} `json:"Result"`
		} `json:"View"`
	} `json:"Response"`
}

type UserRecommendation struct {
	Id           int     `json:"id"`
	UserID       int     `json:"user_id"`
	Fullname     string  `json:"fullname"`
	BusinessName string  `json:"business_name"`
	Address      string  `json:"address"`
	Long         float64 `json:"long"`
	Lat          float64 `json:"lat"`
	Distance     float64 `json:"distance"`
	DistanceText string  `json:"distance_text"`
	City         string  `json:"city"`
	Image        string  `json:"image"`
	SupplierName string  `json:"supplier_name"`
	Price        float64 `json:"price"`
	ProductName  string  `json:"product_name"`
}

type LocationId struct {
	ProvinceId int32 `json:"province_id"`
	CityId     int32 `json:"city_id"`
	DistrictId int32 `json:"district_id"`
}

type Location struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type SupportSupplier struct {
	ID           int32  `json:"id"`
	UserId       int32  `json:"user_id"`
	BusinessName string `json:"business_name"`
	Description  string `json:"description"`
}

type SupplierRecommend struct {
	UserId     int32 `json:"user_id"`
	SupplierId int32 `json:"supplier_id"`
}

type BusinessProfileData struct {
	ID                  int32  `json:"id"`
	UserID              int32  `json:"user_id"`
	BusinessName        string `json:"business_name"`
	BusinessDescription string `json:"business_description"`
	BusinessType        string `json:"business_type"`
	BusinessChain       string `json:"business_chain"`
	BusinessPicture     string `json:"business_picture"`
	TotalEmployee       int32  `json:"total_employee"`
	MonthlyIncome       int32  `json:"monthly_income"`
	Website             string `json:"website"`
	Facebook            string `json:"facebook"`
	Instagram           string `json:"instagram"`
	Shopee              string `json:"shopee"`
	Tokopedia           string `json:"tokopedia"`
	Bukalapak           string `json:"bukalapak"`
}

type BusinessSocmed struct {
	ID         int    `json:"id"`
	URL        string `json:"url"`
	Name       string `json:"name"`
	BusinessID int    `json:"business_id"`
}
