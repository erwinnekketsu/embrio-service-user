package entity

type Register struct {
	UserData     UserData     `json:"user_data"`
	BusinessData BusinessData `json:"business_data"`
	UserFile     UserFile     `json:"user_file"`
}

type UserData struct {
	Fullname      string `json:"fullname"`
	Username      string `json:"username"`
	PhoneNumber   string `json:"phone_number"`
	Email         string `json:"email"`
	AccountNumber string `json:"account_number"`
	Password      string `json:"password"`
	Timezone      string `json:"timezone"`
}

type BusinessData struct {
	BusinessName     string  `json:"business_name"`
	BusinessType     string  `json:"business_type"`
	BusinessAddress  string  `json:"business_address"`
	Province         int     `json:"province"`
	CityDistirct     int     `json:"city_distirct"`
	District         int     `json:"district"`
	Postcode         int     `json:"postcode"`
	Long             float32 `json:"long"`
	Lat              float32 `json:"lat"`
	ProvinceText     string  `json:"province_text"`
	CityDistirctText string  `json:"city_district_text"`
	DistrictText     string  `json:"district_text"`
	PinLocation      string  `json:"pin_location"`
}

type UserFile struct {
	Siup        string `json:"siup"`
	Tdp         string `json:"tdp"`
	Npwp        string `json:"npwp"`
	SupportFile string `json:"support_file"`
}

type UserToken struct {
	Token string `json:"token"`
}
