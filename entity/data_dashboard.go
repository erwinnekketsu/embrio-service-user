package entity

type DataDashboard struct {
	TotalStore   int64     `json:"total_store"`
	TotalProduct int64     `json:"total_product"`
	TotalUser    int64     `json:"total_user"`
	DateStore    []string  `json:"date_store"`
	DateProduct  []string  `json:"date_product"`
	DateUser     []string  `json:"date_user"`
	DataStore    []float32 `json:"data_store"`
	DataProduct  []float32 `json:"data_product"`
	DataUser     []float32 `json:"data_user"`
}
