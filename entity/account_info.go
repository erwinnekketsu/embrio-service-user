package entity

type AccountInfo struct {
	AccountNumber string `json:"account_number"`
	AccountName   string `json:"account_name"`
	BankAccount   string `json:"bank_account"`
}

type UpdateAccountInfo struct {
	UserID        int    `json:"user_id"`
	AccountNumber int64  `json:"account_number"`
	AccountName   string `json:"account_name"`
	BankAccount   string `json:"bank_account"`
}
